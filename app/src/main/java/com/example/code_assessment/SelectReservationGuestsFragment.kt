package com.example.code_assessment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.example.code_assessment.databinding.FragmentReservationGuestListBinding
import com.example.code_assessment.di.ActivityScope
import com.example.code_assessment.viewmodel.SelectReservationGuestFlowViewModel
import javax.inject.Inject

@ActivityScope
class SelectReservationGuestsFragment : Fragment() {

    @Inject
    lateinit var guestListViewModel: SelectReservationGuestFlowViewModel

    private var _binding: FragmentReservationGuestListBinding? = null

    private lateinit var stickyHeaderDecoration: StickyHeaderDecoration

    // This property is only valid between onCreateView and onDestroyView.
    private val binding get() = _binding!!

    override fun onAttach(context: Context) {
        super.onAttach(context)

        (activity as MainActivity).reservationGuestComponent.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentReservationGuestListBinding.inflate(inflater, container, false)
        context ?: return binding.root

        _binding!!.viewModel = guestListViewModel

        val adapter = SelectReservationGuestsRecyclerViewAdapter()
        binding.itemList.adapter = adapter
        subscribeUi(adapter)

        stickyHeaderDecoration = StickyHeaderDecoration(adapter, binding.itemList)
        binding.itemList.addItemDecoration(stickyHeaderDecoration)

        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()

        _binding!!.itemList.removeItemDecoration(stickyHeaderDecoration)
        _binding = null
    }

    private fun subscribeUi(adapter: SelectReservationGuestsRecyclerViewAdapter) {
        guestListViewModel.data.observe(viewLifecycleOwner) { viewData ->
            adapter.headerPositionsDescending = viewData.headerPositionsDescending
            adapter.submitList(viewData.listData)
        }

        guestListViewModel.reviewReservationData.observe(viewLifecycleOwner) { data ->
            if (data.isNotEmpty()) {
                guestListViewModel.clearReviewData()
                continueToReview()
            }
        }
    }

    private fun continueToReview() {
        view?.findNavController()?.navigate(R.id.review_reservation_fragment)
    }
}
