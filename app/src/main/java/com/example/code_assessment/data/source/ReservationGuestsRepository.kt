package com.example.code_assessment.data.source

import com.example.code_assessment.data.ReservationGuestInfo

interface ReservationGuestsRepository {
    suspend fun getGuests(): List<ReservationGuestInfo>
}
