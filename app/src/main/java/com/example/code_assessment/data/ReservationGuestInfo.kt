package com.example.code_assessment.data

data class ReservationGuestInfo constructor(
    val guestInfo: GuestInfo,
    var hasReservation: Boolean
)
