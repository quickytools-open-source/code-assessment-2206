package com.example.code_assessment.data

data class GuestInfo constructor(
    val id: String,
    val firstName: String,
    val lastName: String,
) {
    val displayName get() = "$firstName $lastName"

    override fun toString(): String = "$id $displayName"
}
