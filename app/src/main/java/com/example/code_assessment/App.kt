package com.example.code_assessment

import android.app.Application
import com.example.code_assessment.di.AppComponent
import com.example.code_assessment.di.DaggerAppComponent

class App : Application() {
    val appComponent: AppComponent by lazy {
        DaggerAppComponent.factory().create(applicationContext)
    }
}
