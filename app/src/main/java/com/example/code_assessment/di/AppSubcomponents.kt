package com.example.code_assessment.di

import dagger.Module

@Module(subcomponents = [ReservationGuestsComponent::class])
class AppSubcomponents
