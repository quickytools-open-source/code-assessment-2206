package com.example.code_assessment.di

import com.example.code_assessment.data.source.ReservationGuestsRepository
import com.example.code_assessment.data.source.local.StaticTextGuestRepository
import dagger.Binds
import dagger.Module

@Module
abstract class RepositoryModule {
    // The static text repository is purely for demo. A production app would use a networked repository.
    @Binds
    abstract fun provideReservationGuestRepository(repository: StaticTextGuestRepository): ReservationGuestsRepository
}
