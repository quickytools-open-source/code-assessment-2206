package com.example.code_assessment.di

import com.example.code_assessment.ReviewReservationGuestsFragment
import com.example.code_assessment.SelectReservationGuestsFragment
import dagger.Subcomponent

@ActivityScope
@Subcomponent
interface ReservationGuestsComponent {

    @Subcomponent.Factory
    interface Factory {
        fun create(): ReservationGuestsComponent
    }

    fun inject(fragment: SelectReservationGuestsFragment)
    fun inject(fragment: ReviewReservationGuestsFragment)
}
