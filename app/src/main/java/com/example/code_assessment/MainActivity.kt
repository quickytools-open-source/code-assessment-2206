package com.example.code_assessment

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.code_assessment.databinding.ActivityMainBinding
import com.example.code_assessment.di.ReservationGuestsComponent

class MainActivity : AppCompatActivity() {

    lateinit var reservationGuestComponent: ReservationGuestsComponent

    override fun onCreate(savedInstanceState: Bundle?) {
        reservationGuestComponent =
            (application as App).appComponent.reservationGuestComponent().create()

        super.onCreate(savedInstanceState)

        val binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}
