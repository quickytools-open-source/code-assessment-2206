package com.example.code_assessment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.code_assessment.databinding.FragmentReviewReservationBinding
import com.example.code_assessment.di.ActivityScope
import com.example.code_assessment.viewmodel.SelectReservationGuestFlowViewModel
import javax.inject.Inject

@ActivityScope
class ReviewReservationGuestsFragment : Fragment() {

    @Inject
    lateinit var guestListViewModel: SelectReservationGuestFlowViewModel

    private var _binding: FragmentReviewReservationBinding? = null

    // This property is only valid between onCreateView and onDestroyView.
    private val binding get() = _binding!!

    override fun onAttach(context: Context) {
        super.onAttach(context)

        (activity as MainActivity).reservationGuestComponent.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentReviewReservationBinding.inflate(inflater, container, false)
        context ?: return binding.root

        _binding!!.viewModel = guestListViewModel

        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
