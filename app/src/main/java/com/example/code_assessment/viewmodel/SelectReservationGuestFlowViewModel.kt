package com.example.code_assessment.viewmodel

import androidx.databinding.Observable
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.code_assessment.R
import com.example.code_assessment.data.ReservationGuestInfo
import com.example.code_assessment.data.source.ReservationGuestsRepository
import com.example.code_assessment.di.ActivityScope
import kotlinx.coroutines.launch
import javax.inject.Inject

@ActivityScope
class SelectReservationGuestFlowViewModel @Inject constructor(
    private val guestsRepository: ReservationGuestsRepository
) : ViewModel() {
    companion object {
        const val HEADER_VIEW_TYPE = 1
        const val GUEST_VIEW_TYPE = 2
        const val INFO_VIEW_TYPE = 3

        @JvmStatic
        val viewToLayoutMap = mapOf(
            HEADER_VIEW_TYPE to R.layout.select_reservation_guest_header_view,
            GUEST_VIEW_TYPE to R.layout.select_reservation_guest_guest_view,
            INFO_VIEW_TYPE to R.layout.select_reservation_guest_notice_view,
        )
    }

    private val _data = MutableLiveData(ViewData(emptyList(), emptyList()))
    private val _reviewReservationData =
        MutableLiveData<Collection<ReservationGuestInfo>>(emptyList())

    private var guestViewModels = mutableListOf<SelectReservationGuestGuestItemViewModel>()
    private var selectedGuests = mutableMapOf<String, ReservationGuestInfo>()
    private var selectedGuestsWithReservation = mutableSetOf<String>()

    private val selectChangeObserver = object : Observable.OnPropertyChangedCallback() {
        override fun onPropertyChanged(observable: Observable, propertyId: Int) {
            val guestViewModel = observable as SelectReservationGuestGuestItemViewModel
            val reservationInfo = guestViewModel.info
            val guestId = reservationInfo.guestInfo.id
            if (guestViewModel.getSelected()) {
                selectedGuests[guestId] = reservationInfo
                if (reservationInfo.hasReservation) {
                    selectedGuestsWithReservation.add(guestId)
                }
            } else {
                selectedGuests.remove(guestId)
                selectedGuestsWithReservation.remove(guestId)
            }

            if (_isReservationWarningVisible &&
                (selectedGuestsWithReservation.isNotEmpty() || selectedGuests.isEmpty())
            ) {
                closeReservationWarning()
            }

            hasSelection.notifyChange()
        }
    }

    val data: LiveData<ViewData>
        get() = _data

    val reviewReservationData: LiveData<Collection<ReservationGuestInfo>>
        get() = _reviewReservationData

    val hasSelection = object : ObservableBoolean() {
        override fun get() = selectedGuests.isNotEmpty()
    }

    private var _isReservationWarningVisible = false
    val isReservationWarningVisible = object : ObservableBoolean() {
        override fun get() = _isReservationWarningVisible
    }

    // Stand in for confirm vs conflict
    val isReservationValid
        get() = hasSelection.get() && selectedGuests.size == selectedGuestsWithReservation.size

    init {
        loadData()
    }

    fun continueWithReservation() {
        if (selectedGuests.isEmpty()) {
            // This should never happen but if does it should log message and send to developers to review code
            return
        }

        if (hasSelection.get() && selectedGuestsWithReservation.isEmpty()) {
            showReservationWarning(true)
        } else {
            _reviewReservationData.postValue(selectedGuests.values)
        }
    }

    fun clearReviewData() {
        _reviewReservationData.postValue(emptyList())
    }

    private fun showReservationWarning(show: Boolean) {
        _isReservationWarningVisible = show
        isReservationWarningVisible.notifyChange()
    }

    fun closeReservationWarning() = showReservationWarning(false)

    // Usually UI would be blocked every time data is loading but is not worth it for this sample
    private fun loadData() {
        viewModelScope.launch {
            val guests = guestsRepository.getGuests()
            val viewData = processDataForView(guests)
            _data.postValue(viewData)
        }
    }

    private fun processDataForView(guestsData: List<ReservationGuestInfo>): ViewData {
        unobserveGuestViewModels()

        val guestsByReservation = guestsData.groupBy { it.hasReservation }

        val viewListData = mutableListOf<SelectReservationGuestItemViewModel>()

        // For this demo assume selection resets every time data is loaded
        selectedGuests = mutableMapOf()
        selectedGuestsWithReservation = mutableSetOf()
        val headerPositions = mutableListOf<Int>()

        fun populateGuests(b: Boolean) {
            val guests = guestsByReservation[b]
            if (guests?.isNotEmpty() == true) {
                headerPositions.add(viewListData.size)

                val headerTextResId =
                    if (b) R.string.select_reservation_group_has_res
                    else R.string.select_reservation_group_no_res
                viewListData.add(
                    SelectReservationGuestTextItemViewModel(
                        headerTextResId,
                        HEADER_VIEW_TYPE
                    )
                )
                guests.forEach {
                    val guestViewModel = SelectReservationGuestGuestItemViewModel(it)

                    guestViewModel.addOnPropertyChangedCallback(selectChangeObserver)
                    guestViewModels.add(guestViewModel)

                    viewListData.add(guestViewModel)
                }
            }
        }

        populateGuests(true)
        populateGuests(false)

        if (viewListData.isNotEmpty()) {
            viewListData.add(
                SelectReservationGuestTextItemViewModel(
                    R.string.same_reservation_party_notice,
                    INFO_VIEW_TYPE
                )
            )
        }

        headerPositions.sortDescending()

        return ViewData(viewListData, headerPositions)
    }

    private fun unobserveGuestViewModels() {
        val previousViewModels = guestViewModels
        guestViewModels = mutableListOf()

        previousViewModels.forEach {
            it.addOnPropertyChangedCallback(selectChangeObserver)
        }
    }

    data class ViewData(
        val listData: List<SelectReservationGuestItemViewModel>,
        val headerPositionsDescending: List<Int>,
    )
}
