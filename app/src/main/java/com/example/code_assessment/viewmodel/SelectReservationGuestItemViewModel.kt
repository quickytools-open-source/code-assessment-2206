package com.example.code_assessment.viewmodel

import androidx.annotation.LayoutRes
import androidx.annotation.StringRes
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.example.code_assessment.BR
import com.example.code_assessment.data.ReservationGuestInfo

interface SelectReservationGuestItemViewModel {
    @get:LayoutRes
    val layoutId: Int
    val viewType: Int
        get() = 0
}

data class SelectReservationGuestTextItemViewModel(
    @get:StringRes
    val textResId: Int,
    private val itemViewType: Int
) : SelectReservationGuestItemViewModel {
    override val viewType: Int = itemViewType
    override val layoutId: Int = SelectReservationGuestFlowViewModel.viewToLayoutMap[viewType]!!
}

data class SelectReservationGuestGuestItemViewModel(
    val info: ReservationGuestInfo,
    private var isSelected: Boolean = false,
) : BaseObservable(), SelectReservationGuestItemViewModel {
    override val viewType: Int = SelectReservationGuestFlowViewModel.GUEST_VIEW_TYPE
    override val layoutId: Int = SelectReservationGuestFlowViewModel.viewToLayoutMap[viewType]!!

    @Bindable
    fun getSelected(): Boolean {
        return isSelected
    }

    fun setSelected(value: Boolean) {
        if (value != isSelected) {
            isSelected = value
            notifyPropertyChanged(BR.selected)
        }
    }

    fun toggleSelection() {
        setSelected(!isSelected)
    }
}
