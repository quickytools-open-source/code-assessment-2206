package com.example.code_assessment

import android.content.Context
import android.content.res.Resources
import android.graphics.Canvas
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.example.code_assessment.databinding.SelectReservationGuestStickyHeaderViewBinding
import com.example.code_assessment.viewmodel.SelectReservationGuestFlowViewModel
import com.example.code_assessment.viewmodel.SelectReservationGuestGuestItemViewModel
import com.example.code_assessment.viewmodel.SelectReservationGuestItemViewModel
import com.example.code_assessment.viewmodel.SelectReservationGuestTextItemViewModel

class SelectReservationGuestsRecyclerViewAdapter :
    ListAdapter<SelectReservationGuestItemViewModel, SelectReservationGuestsRecyclerViewAdapter.BindingViewHolder>(
        SelectReservationGuestDiffCallback()
    ) {

    var headerPositionsDescending = emptyList<Int>()

    override fun getItemViewType(position: Int): Int = getItem(position).viewType

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): SelectReservationGuestsRecyclerViewAdapter.BindingViewHolder {
        val binding: ViewDataBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            SelectReservationGuestFlowViewModel.viewToLayoutMap[viewType]!!,
            parent,
            false
        )
        return BindingViewHolder(binding)
    }

    override fun onBindViewHolder(holder: BindingViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    internal fun getHeaderDataBefore(position: Int): Pair<Int, SelectReservationGuestTextItemViewModel>? {
        if (headerPositionsDescending.isNotEmpty() && position < itemCount) {
            for (headerPosition in headerPositionsDescending) {
                if (position >= headerPosition) {
                    val itemData = getItem(headerPosition)
                    if (itemData is SelectReservationGuestTextItemViewModel) {
                        return Pair(headerPosition, itemData)
                    }
                }
            }
        }

        return null
    }

    internal fun isHeaderPosition(position: Int) = headerPositionsDescending.contains(position)

    inner class BindingViewHolder(private val binding: ViewDataBinding) : ViewHolder(binding.root) {
        fun bind(itemViewModel: SelectReservationGuestItemViewModel) {
            binding.setVariable(BR.itemViewModel, itemViewModel)
        }
    }
}

class StickyHeaderDecoration(
    private val adapter: SelectReservationGuestsRecyclerViewAdapter,
    private val recyclerView: RecyclerView,
) : ViewTreeObserver.OnGlobalLayoutListener, RecyclerView.ItemDecoration() {

    private val context: Context
        get() = recyclerView.context

    private val _binding by lazy {
        SelectReservationGuestStickyHeaderViewBinding.inflate(LayoutInflater.from(context))
    }

    private val stickyViewReverseOffset by lazy {
        getPixels(16).toFloat()
    }

    private val headerView: View
        get() = _binding.root

    init {
        recyclerView.viewTreeObserver.addOnGlobalLayoutListener(this)
    }

    override fun onGlobalLayout() {
        recyclerView.viewTreeObserver.removeOnGlobalLayoutListener(this)

        val widthSpec =
            View.MeasureSpec.makeMeasureSpec(recyclerView.width, View.MeasureSpec.EXACTLY)
        val heightSpec =
            View.MeasureSpec.makeMeasureSpec(recyclerView.height, View.MeasureSpec.UNSPECIFIED)

        headerView.measure(widthSpec, heightSpec)

        headerView.layout(0, 0, headerView.measuredWidth, headerView.measuredHeight)
    }

    private fun getPixels(dipValue: Int): Int {
        val r: Resources = context.resources
        return TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            dipValue.toFloat(),
            r.displayMetrics
        ).toInt()
    }

    override fun onDrawOver(canvas: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        super.onDrawOver(canvas, parent, state)

        val firstVisibleView = parent.getChildAt(0)
        val firstViewAdapterPosition = parent.getChildAdapterPosition(firstVisibleView)
        if (firstViewAdapterPosition != RecyclerView.NO_POSITION) {
            val stickyHeaderData = adapter.getHeaderDataBefore(firstViewAdapterPosition)
            stickyHeaderData?.let {
                _binding.header.setText(it.second.textResId)

                val topOffset = getStickyDrawTopOffset(
                    parent,
                    it.first,
                    firstVisibleView,
                    firstViewAdapterPosition
                )
                canvas.drawStickyHeader(topOffset)
            }
        }
    }

    private fun getStickyDrawTopOffset(
        parent: RecyclerView,
        topHeaderAdapterPosition: Int,
        firstVisibleView: View,
        firstViewAdapterPosition: Int,
    ): Float {
        if (topHeaderAdapterPosition == firstViewAdapterPosition) {
            return 0f
        }

        var nextHeaderViewTop = Int.MAX_VALUE
        if (firstViewAdapterPosition > 0 && adapter.isHeaderPosition(
                firstViewAdapterPosition
            )
        ) {
            nextHeaderViewTop = firstVisibleView.top
        } else {
            val secondView = parent.getChildAt(1)
            val secondViewAdapterPosition = parent.getChildAdapterPosition(secondView)
            if (adapter.isHeaderPosition(secondViewAdapterPosition)) {
                nextHeaderViewTop = secondView.top
            }
        }
        val viewVerticalOverlap = nextHeaderViewTop - headerView.height
        return if (viewVerticalOverlap > 0) 0f else viewVerticalOverlap + stickyViewReverseOffset
    }

    private fun Canvas.drawStickyHeader(topOffset: Float) {
        save()
        translate(0f, topOffset)
        headerView.draw(this)
        restore()
    }
}

private class SelectReservationGuestDiffCallback :
    DiffUtil.ItemCallback<SelectReservationGuestItemViewModel>() {

    override fun areItemsTheSame(
        oldItem: SelectReservationGuestItemViewModel,
        newItem: SelectReservationGuestItemViewModel
    ): Boolean {
        if (oldItem.viewType != newItem.viewType) {
            return false
        }

        if (oldItem is SelectReservationGuestTextItemViewModel &&
            newItem is SelectReservationGuestTextItemViewModel
        ) {
            return oldItem.textResId == newItem.textResId
        }

        return false
    }

    override fun areContentsTheSame(
        oldItem: SelectReservationGuestItemViewModel,
        newItem: SelectReservationGuestItemViewModel
    ): Boolean {
        if (oldItem.viewType != newItem.viewType) {
            return false
        }

        if (oldItem is SelectReservationGuestTextItemViewModel &&
            newItem is SelectReservationGuestTextItemViewModel
        ) {
            return oldItem.textResId == newItem.textResId
        }

        if (oldItem is SelectReservationGuestGuestItemViewModel &&
            newItem is SelectReservationGuestGuestItemViewModel
        ) {
            return oldItem.info.guestInfo == newItem.info.guestInfo
        }

        return false
    }
}
